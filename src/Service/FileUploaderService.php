<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 20.01.2018
 * Time: 13:13
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Serwis do uploadu plików
 *
 * Class FileUploaderService
 * @package App\Service
 */
class FileUploaderService
{
    private $targetDir;

    /**
     * FileUploaderService constructor.
     * @param $targetDir
     */
    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDir(), $fileName);

        return $fileName;
    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }
}