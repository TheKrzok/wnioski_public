<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 19.01.2018
 * Time: 14:22
 */

namespace App\Service;

/**
 * Class MailerService
 * @package App\Service
 */
class MailerService
{
    private $mailer;

    /**
     * Konstruktor
     *
     * MailerService constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    /**
     * Funkcja wysyłająca maile,
     * zwraca 'true' gdy mail zostanie pomyślnie wysłany
     * w przeciwnym wypadku zwraca 'false'
     *
     * @param $subject
     * @param $to
     * @param $body
     * @return bool
     */
    public function sendMail($subject, $to, $body)
    {
        $msg = new \Swift_Message($subject, $body, 'text/html', 'utf-8');
        $msg->setTo($to);
        $msg->setFrom('info@urlopy.arpus.pl');
        return $this->mailer->send($msg) > 0;
    }
}