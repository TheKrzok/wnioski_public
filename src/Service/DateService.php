<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 18.01.2018
 * Time: 14:11
 */

namespace App\Service;

use DateInterval;
use DateTime;

/**
 * Serwis zawierający metody związane z datami
 *
 * Class DateService
 * @package App\Service
 */
class DateService
{
    /**
     * Funkcja zwracająca obiekt DateTime z czasem obecnym + miesiąc
     * na potrzeby wygasania hasła
     * @return DateTime
     * @throws \Exception
     */
    public function getNowPlusMonth()
    {
        $now = new DateTime();
        $nowPlusMonth = $now->add(new DateInterval('P1M'));
        return $nowPlusMonth;
    }

    /**
     * Funkcja zwarcająca obecny rok jako int
     * @return int
     */
    public function getCurrentYear()
    {
        $now = new DateTime();
        $year = $now->format('Y');
        return $year;
    }
}