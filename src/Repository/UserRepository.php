<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllSuperiors()
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.userRole', 'ur')
            ->where('ur.role = :role')
            ->setParameter('role', 'ROLE_SUPERIOR')
            ->orderBy('u.surname', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAbsenceLimitsYear($year)
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.absenceLimit', 'al')
            ->where('al.year = :year')
            ->setParameter('year', $year)
            ->orderBy('al.type', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAbsenceLimit($userId, $typeId)
    {
            return $this
                ->createQueryBuilder('u')
                ->leftJoin('u.absenceLimit', 'al')
                ->where('u.id = :userId')
                ->setParameter('userId', $userId)
                ->andWhere('al.type = :typeId')
                ->setParameter('typeId', $typeId)
                ->select('SUM(al.limitValue) AS limitsSum')
                ->getQuery()
                ->getSingleScalarResult();
    }
}
