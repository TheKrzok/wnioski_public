<?php

namespace App\Repository;

use App\Entity\Absence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AbsenceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Absence::class);
    }

    public function getUserAbsenceSum($userId, $typeId=0, $year=0, $statusId=0)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->addSelect('SUM(a.days) AS sum, a.year, at.id AS typeId, ast.name AS statusName');
        $qb->andWhere('a.user = :userId')->setParameter('userId', $userId);

        if($typeId != 0)    $qb->andWhere('a.type = :typeId')->setParameter('typeId', $typeId);
        if($year != 0)      $qb->andWhere('a.year = :year')->setParameter('year', $year);
        if($statusId != 0)  $qb->andWhere('as.id = :statusId')->setParameter('statusId', $statusId);

        $qb->andWhere("ast.name != 'rejected'");
        $qb->addGroupBy('a.year');
        $qb->addGroupBy('at.id');
        $qb->addGroupBy('ast.name');
        $qb->addOrderBy('a.year', 'ASC');
        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function getUserAbsenceCount($userId, $typeId=0, $year=0, $statusName='')
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->addSelect('COUNT(a.days) AS count');
        $qb->andWhere('a.user = :userId')->setParameter('userId', $userId);

        if($typeId != 0)
        {
            $qb->addSelect('at.id AS typeId');
            $qb->andWhere('a.type = :typeId')->setParameter('typeId', $typeId);
            $qb->addGroupBy('at.id');
        }

        if($year != 0)
        {
            $qb->addSelect('a.year');
            $qb->andWhere('a.year = :year')->setParameter('year', $year);
            $qb->addGroupBy('a.year');
            $qb->addOrderBy('a.year', 'ASC');
        }

        if($statusName != '')
        {
            $qb->addSelect('ast.name AS statusName');
            $qb->andWhere('ast.name = :statusName')->setParameter('statusName', $statusName);
            $qb->addGroupBy('ast.name');
        }
        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function findAbsences($constraints)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->addSelect('a');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->join('a.user', 'u');

        if(isset($constraints['superior'])) {
            $qb->andWhere('u.superior = :superior')->setParameter('superior', $constraints['superior']);
        }

        if(isset($constraints['year'])) {
            $qb->andWhere('a.year = :year')->setParameter('year', $constraints['year']);
        }

        if(isset($constraints['type'])) {
            $qb->andWhere('at.id = :type')->setParameter('type', $constraints['type']);
        }

        if(isset($constraints['status'])) {
            $qb->andWhere('ast.id = :status')->setParameter('status', $constraints['status']);
        }

        if(isset($constraints['user'])) {
            $qb->andWhere('a.user = :user')->setParameter('user', $constraints['user']);
        }

        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function findUserAbsencesBetweenDates($userId, $fromDate, $toDate)
    {
        return $this
            ->createQueryBuilder('a')
            ->addSelect('a')
            ->setParameter('fromDate', $fromDate)
            ->setParameter('toDate', $toDate)
            ->setParameter('userId', $userId)
            ->andWhere('a.user = :userId')
            ->andWhere('a.fromDate BETWEEN :fromDate AND :toDate OR a.toDate BETWEEN :fromDate AND :toDate OR :fromDate BETWEEN a.fromDate AND a.toDate OR :toDate BETWEEN a.fromDate AND a.toDate')
            ->getQuery()
            ->getResult()
            ;
    }
}
