<?php

namespace App\Application\UserModule\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Kontroler akcji związanych z profilem użytkownika
 *
 * Class UserProfileController
 * @package App\Controller\SecurityModule
 */
class ProfileController extends AbstractController
{
    /**
     * Akcja wyświetle nia profilu użytkownika
     *
     * @Route("user/profile", name="user_profile")
     * @return Response
     */
    public function viewProfileAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();

        return $this->render("@UserModule/view_profile.html.twig", array('user' => $user));
    }
}