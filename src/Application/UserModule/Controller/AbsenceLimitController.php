<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 30.01.2018
 * Time: 15:14
 */

namespace App\Application\UserModule\Controller;


use App\Entity\Absence;
use App\Entity\AbsenceDict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AbsenceLimitController extends AbstractController
{
    /**
     * @Route("user/absence_limit", name="absence_limit")
     * @return Response
     */
    public function AbsenceLimitAction()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();

        $absenceLimit       = array();
        $absenceLimitSum    = array();
        $absenceDays        = array();

        $em                 = $this->getDoctrine()->getManager();
        $absenceDict        = $em->getRepository(AbsenceDict::class)->findBy(array('limitable' => true));
        $absenceLimitData   = $this->getUser()->getAbsenceLimit();

        if($absenceDict) {
            $absenceUsedSumData = $em->getRepository(Absence::class)->getUserAbsenceSum($user->getId());

            foreach ($absenceUsedSumData AS $aud) {
                switch($aud['statusName']) {

                    case 'accepted':
                        if(!isset($absenceDays['accepted'][$aud['year']][$aud['typeId']])) {
                            $absenceDays['accepted'][$aud['year']][$aud['typeId']] = $aud['sum'];
                        } else {
                            $absenceDays['accepted'][$aud['year']][$aud['typeId']] += $aud['sum'];
                        }

                        if(!isset($absenceDays['acceptedSum'][$aud['typeId']])) {
                            $absenceDays['acceptedSum'][$aud['typeId']] = $aud['sum'];
                        } else {
                            $absenceDays['acceptedSum'][$aud['typeId']] += $aud['sum'];
                        }
                        break;

                    case 'pending':
                        if(!isset($absenceDays['pending'][$aud['year']][$aud['typeId']])) {
                            $absenceDays['pending'][$aud['year']][$aud['typeId']] = $aud['sum'];
                        } else {
                            $absenceDays['pending'][$aud['year']][$aud['typeId']] += $aud['sum'];
                        }

                        if(!isset($absenceDays['pendingSum'][$aud['typeId']])) {
                            $absenceDays['pendingSum'][$aud['typeId']] = $aud['sum'];
                        } else {
                            $absenceDays['pendingSum'][$aud['typeId']] += $aud['sum'];
                        }
                        break;
                }
                (!isset($absenceDays['all'][$aud['year']][$aud['typeId']])) ? $absenceDays['all'][$aud['year']][$aud['typeId']] = $aud['sum'] : $absenceDays['all'][$aud['year']][$aud['typeId']] += $aud['sum'];
                (!isset($absenceDays['allSum'][$aud['typeId']])) ? $absenceDays['allSum'][$aud['typeId']] = $aud['sum'] : $absenceDays['allSum'][$aud['typeId']] += $aud['sum'];
            }

            foreach ($absenceLimitData AS $ald) {
                $typeId = $ald->getType()->getId();
                $limitValue = $ald->getLimitValue();
                $year = $ald->getYear();
                $absenceLimit[$year][$typeId] = $limitValue;
                if(!isset($absenceLimitSum[$typeId])) {
                    $absenceLimitSum[$typeId] = $limitValue;
                } else {
                    $absenceLimitSum[$typeId] += $limitValue;
                }
            }
        }

        return $this->render(
            "@UserModule/absence_limit.html.twig",
            array(
                'absenceDict'       => $absenceDict,
                'absenceLimit'      => $absenceLimit,
                'absenceLimitSum'   => $absenceLimitSum,
                'absenceDays'       => $absenceDays
            )
        );
    }
}