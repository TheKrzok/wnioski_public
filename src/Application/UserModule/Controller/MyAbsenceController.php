<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 10:24
 */

namespace App\Application\UserModule\Controller;


use App\Application\UserModule\Form\AbsenceFilterType;
use App\Entity\Absence;
use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyAbsenceController extends AbstractController
{
    /**
     * @Route("user/my_absence", name="my_absence")
     * @param Request $request
     * @return Response
     */
    public function MyAbsenceAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $em = $this->getDoctrine()->getManager();

        $statusArray    = array();
        $typeArray      = array();

        $curDate = new DateTime();
        $curYear = (int)$curDate->format('Y');

        $statusArray['wszystkie']   = 0;
        $typeArray['wszystkie']     = 0;

        $sa = $em->getRepository(AbsenceStatusDict::class)->findAll();
        foreach($sa AS $s) {
            $statusArray[$s->getDescription()] = $s->getId();
        }

        $ta = $em->getRepository(AbsenceDict::class)->findAll();
        foreach ($ta AS $t) {
            $typeArray[$t->getDescription()] = $t->getId();
        }

        $form = $this->createForm(
            AbsenceFilterType::class,
            array('year' => $curDate),
            array(
                'statusChoices' => $statusArray,
                'typeChoice' => $typeArray,
                'attr' => array(
                    'novalidate' => 'novalidate',
                    'class' => 'form-inline ml-5'
                )
            ));
        $form->handleRequest($request);

        $user                   = $this->getUser();
        $constraints            = array();
        $constraints['user']    = $user;
        $constraints['year']    = $curYear;

        if($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            if($data['status'] !== 0) {
                $constraints['status'] = $data['status'];
            } else {
                unset($constraints['status']);
            }

            if($data['type'] !== 0) {
                $constraints['type'] = $data['type'];
            }

            if($data['year'] !== 0) {
                $constraints['year'] = (int)$data['year']->format('Y');
            }
        }


        $absence = $em->getRepository(Absence::class)->findBy($constraints);

        return $this->render("@UserModule/my_absence.html.twig", array('a' => $absence, 'form' => $form->createView()));
    }
}