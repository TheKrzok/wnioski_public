<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 18:12
 */

namespace App\Application\UserModule\Controller;


use App\Entity\Absence;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class PanelController extends AbstractController
{
    /**
     * @Route("user/user_panel", name="user_panel")
     * @return Response
     */
    public function userPanelAction()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $pendingData        = $em = $this->getDoctrine()->getManager()->getRepository(Absence::class)->getUserAbsenceCount($this->getUser()->getId(),0,0,'pending');
        if($pendingData) {
            $pendingCount       = $pendingData[0]['count'];
        } else {
            $pendingCount = 0;
        }

        $passwordExpiryDate = $this->getUser()->getPasswordExpiryDate();

        return $this->render('@UserModule/user_panel.html.twig', array('passwordExpryDate' => $passwordExpiryDate, 'pendingCount' => $pendingCount));
    }
}