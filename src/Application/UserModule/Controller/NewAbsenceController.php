<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 10:24
 */

namespace App\Application\UserModule\Controller;


use App\Application\UserModule\Form\AbsenceType;
use App\Entity\Absence;
use App\Entity\AbsenceStatusDict;
use App\Entity\User;
use App\Service\MailerService;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class NewAbsenceController
 * @package App\Application\UserModule\Controller
 */
class NewAbsenceController extends AbstractController
{
    /**
     * @Route("user/new_absence", name="new_absence")
     * @param Request $request
     * @param MailerService $mailerService
     * @return Response
     * @throws \Exception
     */
    public function NewAbsenceAction(Request $request, MailerService $mailerService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $absence = new Absence();
        $form = $this->createForm(AbsenceType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $from           = $absence->getFromDate();
            $to             = $absence->getToDate();

            $user           = $this->getUser();
            $userId         = $user->getId();
            $absenceType    = $absence->getType();
            $typeId         = $absenceType->getId();
            $em             = $this->getDoctrine()->getManager();
            $superiorMail   = $user->getSuperior()->getEmail();
            $errors         = array();

            $days = 0;

            $startDate   = clone $from;
            $limitDate   = clone $to;

            for ($startDate; $startDate <= $limitDate; $startDate->add(new DateInterval('P1D'))) {
                $weekday = (int)$startDate->format('N');
                if (!in_array($weekday, array(6, 7,))) {
                    $days++;
                }
            }

            if ($to < $from)
            {
                $errors[] = 'Data do nie może być mniejsza od daty od';
            }
            else
            {
                $currentAbsences = $em->getRepository(Absence::class)->findUserAbsencesBetweenDates($userId, $from, $to);

                if($currentAbsences) {
                    $errors[] = 'Na wybrany okres '.$from->format('d-m-Y').' - '.$to->format('d-m-Y')." istnieją już nieobnecności lub oczekujące wnioski:";
                    foreach ($currentAbsences AS $abs) {
                        $errors[] = '- '.$abs->getType()->getDescription().' od '.$abs->getFromDate()->format('d-m-Y').' do '.$abs->getToDate()->format('d-m-Y');
                    }
                }

                if ($absenceType->getLimitable())
                {
                    $limit  = $em->getRepository(User::class)->getAbsenceLimit($userId, $typeId);

                    if (!$limit)
                    {
                        $errors[] = 'Nie masz ustalonego limitu dla nieobecności "' . $absenceType->getDescription() . '" Skontaktuj się z administratorem';
                    }
                    else
                    {
                        $usedDaysData = $em->getRepository(Absence::class)->getUserAbsenceSum($user->getId(), $absenceType->getId());
                        if($usedDaysData) {
                            $used = $usedDaysData[0]['sum'];
                        } else {
                            $used = 0;
                        }
                        $daysLeft   = $limit - $used;

                        if ($days > $daysLeft) {
                            $errors[] = 'Liczba dni na wniosku przekroczy dostępny limit dla nieobecności \''
                                .$absenceType->getDescription() . '\'.'
                                . ' Dni do wybrania: ' . $limit
                                . ' Dni na wniosku: ' . $days;
                        }
                    }
                } else {
                    $daysLeft = 'brak limitu';
                }

            }

            if(empty($errors))
            {
                $status         = $em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => 'pending'));
                $yerarInt       = (int)$from->format('Y');
                $securityCode   = md5(random_bytes(10));

                $absence->setSecurityCode($securityCode);
                $absence->setYear($yerarInt);
                $absence->setDays($days);
                $absence->setStatus($status);
                $absence->setUser($user);
                $absence->setCreateDate(new DateTime());

                $em->persist($absence);
                $em->flush();

                $acceptanceLink = $this->generateUrl("absence_accept_action", array('securityCode' => $securityCode), UrlGeneratorInterface::ABSOLUTE_URL);
                $rejectLink     = $this->generateUrl("absence_reject_action", array('securityCode' => $securityCode), UrlGeneratorInterface::ABSOLUTE_URL);

                $mailerService->sendMail(
                    'Nowy wniosek urlopowy od '.$user->getSurname().' '.$user->getName(),
                    $superiorMail,
                    $this->render(
                        '@UserModule/mail_superior_absence_action.html.twig',
                        array(
                            'aLink' => $acceptanceLink,
                            'rLink' => $rejectLink,
                            'u' => $user,
                            'a' => $absence,
                            'daysLeft' => $daysLeft
                        )
                    )
                );

                $this->addFlash('success', 'Wniosek został pomyślnie zapisany i oczekuje na zatwierdzenie przez przełożonego.');
            }
            else
            {
                $this->addFlash('error', implode("\n", $errors));
            }

            //return $this->redirect($request->getUri());
        }

        return $this->render("@UserModule/new_absence.html.twig", array('form' => $form->createView()));
    }
}