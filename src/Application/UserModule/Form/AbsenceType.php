<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 30.01.2018
 * Time: 16:57
 */

namespace App\Application\UserModule\Form;


use App\Entity\AbsenceDict;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class AbsenceType
 * @package App\Application\UserModule\Form
 */
class AbsenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', EntityType::class, array(
                'label' => 'Typ nieobecności',
                'class' => AbsenceDict::class,
                'choice_label' => 'description',
            ))
            ->add('fromDate', DateType::class, array(
                'label' => 'Od',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data od nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                    new GreaterThanOrEqual(array(
                        'value' => 'today',
                        'message' => 'Wniosek nie może być złożony na okres który minął'
                    )),
                ),
                'attr' => array(
                    'style' => 'width: auto'
                ),
            ))
            ->add('toDate', DateType::class, array(
                'label' => 'Do',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data do nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                    new GreaterThanOrEqual(array(
                        'value' => 'today',
                        'message' => 'Wniosek nie może być złożony na okres który minął'
                    )),
                ),
                'attr' => array(
                    'style' => 'width: auto'
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Złóż wniosek',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
        ;
    }
}