<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:20
 */

namespace App\Application\AdminModule\Controller;


use App\Service\DateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminPanelController
 * @package App\Application\AdminModule\Controller
 */
class AdminPanelController extends AbstractController
{
    /**
     * @Route("admin/admin_panel", name="admin_panel")
     * @return Response
     */
    public function AdminPanelAction(DateService $dateService)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $absenceLimitsYear = $dateService->getCurrentYear();
        return $this->render("@AdminModule/admin_panel.html.twig", array('absenceLimitsYear' => $absenceLimitsYear));
    }
}