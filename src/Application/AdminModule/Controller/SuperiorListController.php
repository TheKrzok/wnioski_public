<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:58
 */

namespace App\Application\AdminModule\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SuperiorListController
 * @package App\Application\AdminModule\Controller
 */
class SuperiorListController extends AbstractController
{
    /**
     * @Route("admin/superior_list", name="superior_list")
     * @return Response
     */
    public function SuperiorListAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render("@AdminModule/superior_list.html.twig");
    }

}