<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 28.01.2018
 * Time: 13:02
 */

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\AbsenceLimitType;
use App\Application\AdminModule\Form\YearType;
use App\Entity\AbsenceDict;
use App\Entity\AbsenceLimit;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AbsenceLimitsController
 * @package App\Application\AdminModule\Controller
 */
class AbsenceLimitsController extends AbstractController
{
    /**
     * @Route("admin/absence_limits/{year}", name="absence_limits")
     * @param Request $request
     * @param $year
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AbsenceLimitsAction(Request $request, $year)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $dt             = new DateTime();
        $formYear       = $dt::createFromFormat('Y', $year);
        $form           = $this->createForm(YearType::class, array('year' => $formYear, 'status' => 1), array('attr' => array('novalidate' => 'novalidate', 'class' => 'float-left')));
        $absenceLimit   = array();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('absence_limits', array('year' => $form->get('year')->getData()->format('Y')));
        }

        $absenceDict        = $this->getDoctrine()->getRepository(AbsenceDict::class)->findBy(array('limitable' => true));
        $absenceDictCount   = count($absenceDict);

        $userRepo = $this->getDoctrine()->getRepository(User::class);

        $superiorCollection = $userRepo->findAllSuperiors();
        $absenceLimitData   = $userRepo->findAbsenceLimitsYear($year);

        if($absenceLimitData) {
            foreach($absenceLimitData AS $userObj) {
                $abs = $userObj->getAbsenceLimit();
                foreach($abs AS $data) {
                    $absenceLimit[$userObj->getId()][$data->getType()->getId()] = $data->getLimitValue();
                }

            }
        }



        return $this->render(
            "@AdminModule/absence_limits.html.twig",
            array(
                'filterForm' => $form->createView(),
                'superiorCollection' => $superiorCollection,
                'absenceDict' => $absenceDict,
                'absenceDictCount' => $absenceDictCount,
                'absenceLimit' => $absenceLimit,
                'year' => $year,
            )
        );
    }

    /**
     * @Route("admin/absence_limits/set/{year}/{type}/{userId}", name="absence_limits_set")
     * @param Request $request
     * @param $year
     * @param $type
     * @param $userId
     * @return null
     */
    public function AbsenceLimitsSetAction(Request $request, $year, $type, $userId)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $absenceLimit   = new AbsenceLimit();
        $form           = $this->createForm(AbsenceLimitType::class, $absenceLimit, array('attr' => array('novalidate' => 'novalidate')));
        $user           = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('id' => $userId));
        $absenceType    = $this->getDoctrine()->getRepository(AbsenceDict::class)->findOneBy(array('id' => $type));

        $userDescription = $user->getSurname().' '.$user->getName();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $absenceLimit->setLimitValue($form->get('limit_value')->getData());
            $absenceLimit->setType($absenceType);
            $absenceLimit->setYear((int)$year);
            $user->addAbsenceLimit($absenceLimit);

            $em->persist($absenceLimit);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("absence_limits", array('year' => $year));
        }

        return $this->render(
            "@AdminModule/absence_limit_set.html.twig",
            array(
                'form' => $form->createView(),
                'year' => $year,
                'absenceType' => $absenceType->getDescription(),
                'userDescription' => $userDescription
            )
        );
    }
}