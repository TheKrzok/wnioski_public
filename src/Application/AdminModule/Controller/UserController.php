<?php

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\UserType;
use App\Entity\User;
use App\Service\DateService;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Kontroler akcji rejestracji w serwisie
 *
 * Class UserController
 * @package App\Controller\SecurityModule
 */
class UserController extends AbstractController
{
    /**
     * Akcja rejestracji w serwisie
     *
     * @Route("admin/user/add/{type}", name="superior_add")
     * @Route("admin/user/add/{type}/{superiorId}", name="user_add")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailerService $mailer
     * @param DateService $dateService
     * @param $type
     * @param int $superiorId
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function addAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerService $mailer, DateService $dateService, $type, $superiorId = 0)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $user               = new User();
        $formDetails        = array('headerName' => 'przełożonego', 'headerAdditional' => NULL);
        $superior           = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(array('id' => $superiorId));
        if($superior) {
            $formDetails['headerName'] = 'użytkownika';
            $formDetails['headerAdditional'] = " jako podwładnego użytkownika ".$superior->getName()." ".$superior->getSurname();
        }

        $form = $this->createForm(UserType ::class, $user, array('attr' => array('novalidate' => 'novalidate')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $temporaryPassword  = 'Arpus_'.random_int(1000,9999);
            $password           = $passwordEncoder->encodePassword($user, $temporaryPassword);
            $birthdateDate      = $user->getBirthdate();
            $passwordExpiryDate = $dateService->getNowPlusMonth();


            $user->setPassword($password);
            $user->setBirthdate($birthdateDate);
            $user->setPasswordExpiryDate($passwordExpiryDate);

            if('superior' === $type) {
                $user->addRole('ROLE_SUPERIOR');
            } else {
                $user->addRole('ROLE_USER');
            }

            if($superior) {
                $user->setSuperior($superior);
            }

            $user->setIsActive(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Użytkownik '.$user->getUsername().' został dodany. Na adres e-mail '.$user->getEmail().' została wysłana wiadomość z linkiem do strony logowania.'
            );

            $mailer->sendMail('Witaj w systemie!', $user->getEmail(), $this->renderView('@AdminModule/hello_mail.html.twig', array('userName' => $user->getUsername(), 'temporaryPassword' => $temporaryPassword)));

            return $this->redirect($request->getUri());
        }

        return $this->render('@AdminModule/user_add.html.twig', array('form' => $form->createView(), 'formDetails' => $formDetails));
    }

    /**
     * @Route("admin/user/edit/{id}", name="user_edit")
     *
     * @param Request $request
     * @param MailerService $mailer
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, MailerService $mailer, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id' => $id));
        $form = $this->createForm(UserType ::class, $user, array('attr' => array('novalidate' => 'novalidate')));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->merge($user);
            $em->flush();

            return $this->redirectToRoute('user_list');
        }
        return $this->render("@AdminModule/user_edit.html.twig", array('form' => $form->createView(), 'userName' => $user->getUsername()));
    }

    /**
     * @Route("admin/user/delete/{id}", name="user_delete")
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id' => $id));
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('user_list');
    }
}