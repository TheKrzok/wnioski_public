<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 25.01.2018
 * Time: 10:49
 */

namespace App\Application\AdminModule\Controller;


use App\Entity\AbsenceDict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AbsenceDictDeleteController
 * @package App\Application\AdminModule\Controller
 */
class AbsenceDictDeleteController extends AbstractController
{
    /**
     * @Route("admin/absence_dict/delete/{id}", name="absence_dict_delete")
     * @param $id
     * @return RedirectResponse
     */
    public  function AbsenceDictDeleteAction($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(AbsenceDict::class)->findOneBy(array('id' => $id));
        $absence->setActive(0);
        $em->merge($absence);
        $em->flush();

        return $this->redirectToRoute("absence_dict");
    }

}