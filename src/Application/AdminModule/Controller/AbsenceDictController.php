<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:44
 */

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\AbsenceAddType;
use App\Entity\AbsenceDict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AbsenceDictController
 * @package App\Application\AdminModule\Controller
 */
class AbsenceDictController extends AbstractController
{
    /**
     * @Route("admin/absence_dict", name="absence_dict")
     * @param Request $request
     * @return Response
     */
    public function AbsenceDictAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $absence = new AbsenceDict();
        $absenceAddForm = $this->createForm(AbsenceAddType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
        $absenceAddForm->handleRequest($request);

        if($absenceAddForm->isSubmitted() && $absenceAddForm->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $absence->setActive(1);
            $em->persist($absence);
            $em->flush();

            return $this->redirect($request->getUri());
        }

        $em = $this->getDoctrine()->getManager();
        $absenceDict = $em->getRepository(AbsenceDict::class)->findBy(array('active' => 1));

        return $this->render("@AdminModule/absence_dict.html.twig", array('absenceDict' => $absenceDict, 'absenceAddForm' => $absenceAddForm->createView()));
    }
}