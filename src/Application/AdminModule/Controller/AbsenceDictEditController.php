<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 25.01.2018
 * Time: 11:02
 */

namespace App\Application\AdminModule\Controller;


use App\Application\AdminModule\Form\AbsenceEditType;
use App\Entity\AbsenceDict;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbsenceDictEditController
 * @package App\Application\AdminModule\Controller
 */
class AbsenceDictEditController extends AbstractController
{
    /**
     * @Route("admin/absence_dict/edit/{id}", name="absence_dict_edit")
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function AbsenceDictEditAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(AbsenceDict::class)->findOneBy(array('id' => $id));
        $form = $this->createForm(AbsenceEditType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em->merge($absence);
            $em->flush();

            return $this->redirectToRoute('absence_dict');
        }

        return $this->render("@AdminModule/absence_dict_edit.html.twig", array('absence' => $absence, 'form' => $form->createView()));
    }
}