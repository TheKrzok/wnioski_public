<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:58
 */

namespace App\Application\AdminModule\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WorkerListController
 * @package App\Application\AdminModule\Controller
 */
class UserListController extends AbstractController
{
    /**
     * @Route("admin/user_list", name="user_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function UserListAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $superiorCollection = $userRepo->findAllSuperiors();

        return $this->render("@AdminModule/user_list.html.twig", array('superiorCollection' => $superiorCollection));
    }
}