<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 28.01.2018
 * Time: 14:57
 */

namespace App\Application\AdminModule\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbsenceLimitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('limit_value', IntegerType::class, array(
                'label' => 'Limit dni',
                'constraints' => array(
                    new NotBlank(array('message' => 'Limit nie może być pusty'))
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Ustaw',
                'attr' => array('class' => 'btn btn-success')
            ))
        ;
    }
}