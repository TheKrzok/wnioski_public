<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 25.01.2018
 * Time: 08:56
 */

namespace App\Application\AdminModule\Form;


use App\Entity\AbsenceDict;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbsenceAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, array(
                'label' => 'Opis nieobecności',
                'constraints' => array(
                    new Length(array(
                        'min' => 4,
                        'minMessage' => 'Opis nie może być krótszy niż 4 znaki',
                        'max' => 30,
                        'maxMessage' => 'Opis nie może być dłuższy niż 30 znaków'
                    )),
                    new NotBlank(array(
                        'message' => 'Opis nie może być pusty'
                    ))
                )
            ))
            ->add('limitable', CheckboxType::class, array(
                'label' => 'Czy ma mieć limit?',
                'attr' => array('checked' => 'true'),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array('class' => 'btn btn-success'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AbsenceDict::class,
        ));
    }
}