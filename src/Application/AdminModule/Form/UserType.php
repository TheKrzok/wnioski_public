<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 13.01.2018
 * Time: 19:17
 */

namespace App\Application\AdminModule\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Klasa budująca formularz rejestracyjny
 *
 * Class RegisterType
 * @package App\Form\SecurityModule
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Imię',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Imię nie może być puste')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Imię nie może być krótsze niż 2 znaki',
                        'max' => 30, 'maxMessage' => 'iImię nie może być dłuższe niż 50 znaków'))
                )
            ))
            ->add('surname', TextType::class, array(
                'label' => 'Nazwisko',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Nazwisko nie może być puste')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Nazwisko nie może być krótsze niż 2 znaki',
                        'max' => 50, 'maxMessage' => 'Nazwisko nie może być dłuższe niż 50 znaków')),
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Adres e-mail',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Adres E-mail nie może być pusty')),
                    new Email(array('message' => 'Podana wartość nie jest poprawnym adresem e-mail')),
                    new Length(array(
                        'min' => 4, 'minMessage' => 'E-mail nie może być krótszy niż 4 znaki',
                        'max' => 50, 'maxMessage' => 'E-mail nie może być dłuższy niż 50 znaków'
                    ))
                )
            ))
            ->add('pesel', TextType::class, array(
                'label' => 'Numer pesel',
                'constraints' => array(
                    new NotBlank(array('message' => 'Pesel nie może być pusty')),
                    new Length(array(
                        'min' => 11,
                        'max' => 11,
                        'exactMessage' => 'Numer pesel musi mieć dokładnie 11 znaków'
                    )),
                    new Regex(array(
                        'pattern' => '/[0-9]{11}/',
                        'match' => true,
                        'message' => 'Numer pesel może składać się wyłącznie z liczb'
                    ))
                )
            ))
            ->add('birthdate', DateType::class, array(
                'label' => 'Data urodzenia',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data urodzenia nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                )
            ))
            ->add('username', TextType::class, array(
                'label' => 'Nazwa użytkownika',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Nazwa użytkownika nie może być pusta')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Nazwa uzytkownika nie może być krótsza niż 3 znaki',
                        'max' => 20, 'maxMessage' => 'Nazwa użytkownika nie może być dłuższa niż 20 znaków'
                    )),
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}