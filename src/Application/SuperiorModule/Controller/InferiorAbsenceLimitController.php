<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 22:02
 */

namespace App\Application\SuperiorModule\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InferiorAbsenceLimitController
 * @package App\Application\SuperiorModule\Controller
 */
class InferiorAbsenceLimitController extends AbstractController
{
    /**
     * @Route("superior/inferior_absence_limit", name="inferior_absence_limit")
     * @return Response
     */
    public function inferiorAbsenceLimitAction()
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERIOR');

        return $this->render("@SuperiorModule/inferior_absence_limit.html.twig");
    }
}