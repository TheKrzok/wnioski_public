<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 20:39
 */

namespace App\Application\SuperiorModule\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PanelController
 * @package App\Application\SuperiorModule\Controller
 */
class InferiorListController extends AbstractController
{
    /**
     * @Route("superior/inferior_list", name="inferior_list")
     * @return Response
     */
    public function inferiorListAction()
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERIOR');

        $user       = $this->getUser();
        $inferior   = $user->getInferior();

        return $this->render("@SuperiorModule/inferior_list.html.twig", array('i' => $inferior));
    }

}