<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 18:18
 */

namespace App\Application\SuperiorModule\Controller;


use App\Application\SecurityModule\Form\AbsenceRejectReasonType;
use App\Entity\Absence;
use App\Entity\AbsenceStatusDict;
use App\Service\MailerService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AbsenceController extends AbstractController
{
    /**
     * @Route("superior/absence_action/accept/{id}", name="superior_accept_action")
     * @param $id
     * @param MailerService $mailerService
     * @return Response
     */
    public function AbsenceAcceptAction($id, MailerService $mailerService)
    {
        $message    = '';
        $alertClass = '';
        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(Absence::class)->find($id);

        if($absence) {
            $statusName = $absence->getStatus()->getName();
            if($statusName == 'pending') {
                $user   = $absence->getUser();
                $em     = $this->getDoctrine()->getManager();

                $absence->setStatus($em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => 'accepted')));
                $absence->setStatusChangeDate(new DateTime());

                $em->merge($absence);
                $em->flush();

                $mailerService->sendMail('Akceptacja wniosku', $user->getEmail(), $this->render('@SuperiorModule/mail_absence_status_change.html.twig', array('absence' => $absence)));
                $message = "Wniosek został zaakceptowany. Na adres użytkownika ".$user->getSurname()." ".$user->getName()." została wysłana informacja o zmianie statusu wniosku";
                $alertClass = 'alert-success';

            } else {
                $message = "Podany wniosek jest w chwili obecnej na statusie ".$absence->getStatus()->getDescription().".
                Link jest nieaktualny, status pozostaje niezmieniony.
                Można usunąć wiadomosć zawierającą ten link";
            }
        } else {
            $message = "Nie znaleziono wniosku. Skontaktuj się z administratoerm.";
            $alertClass = 'alert-danger';
        }

        return $this->render('@SecurityModule/absence_action.html.twig', array('message' => $message, 'alertClass' => $alertClass, 'back' => true));
    }

    /**
     * @Route("superior/absence_action/reject/{id}", name="superior_reject_action")
     * @param Request $request
     * @param $id
     * @param MailerService $mailerService
     * @return mixed
     */
    public function AbsenceRejectAction(Request $request, $id, MailerService $mailerService)
    {
        $message    = '';
        $alertClass = '';
        $formView   = NULL;
        $em         = $this->getDoctrine()->getManager();
        $absence    = $em->getRepository(Absence::class)->find($id);


        if($absence) {
            $statusName = $absence->getStatus()->getName();
            if($statusName == 'pending') {
                $form = $this->createForm(AbsenceRejectReasonType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
                $form->handleRequest($request);
                $formView = $form->createView();

                if($form->isSubmitted() && $form->isValid())
                {
                    $user = $absence->getUser();
                    $em = $this->getDoctrine()->getManager();
                    $absence->setStatus($em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => 'rejected')));
                    $absence->setStatusChangeDate(new DateTime());
                    $em->merge($absence);
                    $em->flush();

                    $mailerService->sendMail('Odrzucenie wniosku', $user->getEmail(), $this->render('@SuperiorModule/mail_absence_status_change.html.twig', array('absence' => $absence)));
                    $message = "Wniosek został odrzucony. Na adres użytkownika ".$user->getSurname()." ".$user->getName()." została wysłana informacja o zmianie statusu wniosku";
                    $alertClass = 'alert-danger';
                }
            } else {
                $message = "Podany wniosek jest w chwili obecnej na statusie ".$absence->getStatus()->getDescription().".
                Link jest nieaktualny, status pozostaje niezmieniony.
                Można usunąć wiadomosć zawierającą ten link";
                $alertClass = 'alert-info';
            }
        } else {
            $message = "Nie znaleziono wniosku. Skontaktuj się z administratoerm.";
            $alertClass = 'alert-danger';
        }

        return $this->render('@SecurityModule/absence_action.html.twig', array('message' => $message, 'alertClass' => $alertClass, 'form' => $formView, 'back' => true));
    }

}