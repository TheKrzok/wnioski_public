<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 20:39
 */

namespace App\Application\SuperiorModule\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PanelController
 * @package App\Application\SuperiorModule\Controller
 */
class PanelController extends AbstractController
{
    /**
     * @Route("superior/superior_panel", name="superior_panel")
     * @return Response
     */
    public function superiorPanelAction()
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERIOR');

        $passwordExpiryDate = $this->getUser()->getPasswordExpiryDate();
        return $this->render("@SuperiorModule/superior_panel.html.twig", array('passwordExpryDate' => $passwordExpiryDate));
    }

}