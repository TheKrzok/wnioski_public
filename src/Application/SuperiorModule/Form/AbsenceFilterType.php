<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 10:53
 */

namespace App\Application\SuperiorModule\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbsenceFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', ChoiceType::class, array(
                'label' => 'Pracownik',
                'choices' => $options['userChoice'],
                'attr' => array(
                    'class' => 'mr-0'
                )
            ))
            ->add('year', DateType::class, array(
                'label' => 'Rok',
                'label_attr' => array('class' => 'text-right'),
                'widget' => 'single_text',
                'format' => 'yyyy',
                'constraints' => array(
                    new NotBlank(array('message' => 'Pole rok nie może być puste'))
                ),
                'attr' => array(
                    'class' => 'mr-3'
                )
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Typ',
                'choices' => $options['typeChoice'],
                'attr' => array(
                    'class' => 'mr-5'
                )
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'Status',
                'label_attr' => array('class' => 'text-right'),
                'choices' => $options['statusChoices'],
                'attr' => array(
                    'class' => 'mr-1'
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Filtruj',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'statusChoices' => null,
            'typeChoice'    => null,
            'userChoice'    => null,
        ));
    }
}