<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 18.01.2018
 * Time: 09:01
 */

namespace App\Application\SecurityModule\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Klasa budująca formularz do przypomnienia hasła
 *
 * Class RequestPasswordChangeType
 * @package App\Form\SecurityModule
 */
class RequestPasswordChangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'Podaj swój adres email:',
                'constraints' => array(
                    new NotBlank(),
                    new Email()
                    )
            ))
            ->add('submit', SubmitType::class, array('label' => 'Wyślij link', 'attr' => array('class' => 'btn btn-success')))
            ;
    }
}