<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 15:00
 */

namespace App\Application\SecurityModule\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbsenceRejectReasonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rejectedReason', TextareaType::class, array(
                'label' => 'Powód odrzucenia wniosku',
                'constraints' => array(
                    new notBlank(array('message' => 'Pole nie może być puste')),
                    new Length(array(
                        'min' => 10,
                        'minMessage' => 'Pole musi zawierać conajmniej 10 znaków',
                        'max' => 255,
                        'maxMessage' => 'Pole nie może zazwierać więcej njiż 255 znaków',
                    ))
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Odrzuć wniosek',
                'attr' => array(
                    'class' => 'btn btn-danger'
                )
            ))
        ;
    }

}