<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 18.01.2018
 * Time: 08:47
 */

namespace App\Application\SecurityModule\Controller;

use App\Entity\User;
use App\Application\SecurityModule\Form\RequestPasswordChangeType;
use App\Application\SecurityModule\Form\PasswordChangeType;
use App\Service\DateService;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Kontoler akcji zmiany hasła w przypadku jego zapomnienia
 *
 * Class PasswordChangeController
 * @package App\Controller\SecurityModule
 */
class PasswordChangeController extends AbstractController
{

    /**
     * Akcja wydyłająca link do formularza zmiany hasła na podany przez urzytkownika adres email
     *
     * @Route("security/request_password_change", name="request_pasword_change")
     * @param Request $request
     * @param MailerService $mailer
     * @return Response
     * @throws \Exception
     */
    public function requestPasswordChangeAction(Request $request, MailerService $mailer)
    {
        $form = $this->createForm(RequestPasswordChangeType::class)->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $email  = $form->get('email')->getData();
            $em     = $this->getDoctrine()->getManager();
            $user   = $em->getRepository(User::class)->findOneBy(['email' => $email]);

            if($user) {
                $securityCode   = md5(random_bytes(10));
                $link           = $this->generateUrl("password_change", array('securityCode' => $securityCode), UrlGeneratorInterface::ABSOLUTE_URL);

                $user->setResettingSecurityCode($securityCode);

                $em->merge($user);
                $em->flush();

                $mailer->sendMail('Prośba o zmianę hasła', $user->getEmail(), $this->renderView(
                    '@SecurityModule/password_change_mail.twig',
                    array('name' => $user->getName(), 'link' => $link)
                ));

                if($mailer) {
                    return $this->redirectToRoute("request_password_success");
                } else {
                    $this->addFlash('error', 'Niestety nie udało się wysłać wiadomości z linkiem do zmiany hasła. Skontaktuj się z administratorem systemu');
                }
            } else {
                $form->get('email')->addError(new FormError('Podany adres email nie istnieje'));
            }
        }

        return $this->render("@SecurityModule/request_password_change.html.twig", array("form" => $form->createView()));
    }

    /**
     * Akcja generująca widok w przypadku pomyślnego wysłania maila z linkiem do zmiany hasła
     *
     * @Route("security/request_password_success", name="request_password_success")
     * @return Response
     */
    public function requestPasswordSuccessAction()
    {
        return $this->render("@SecurityModule/request_password_success.html.twig");
    }

    /**
     * Akcja zmiany hasła gdy użytkownik wejdzie na link w wysłanej wiadomości
     *
     * @Route("security/password_change", name="password_change")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param DateService $dateService
     * @return Response
     * @throws \Exception
     */
    public function passwordChangeAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, DateService $dateService)
    {
        $securityCode = $request->query->get("securityCode");

        $em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository(User::class)->findOneBy(array("resettingSecurityCode" => $securityCode));

        if(!$user) {
            throw $this->createNotFoundException('Podany link nie istnieje');
        } else {
            $form = $this->createForm(PasswordChangeType::class);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                $newPassword = $form->get('plainPassword')->getData();
                $passwordExpiryDate = $dateService->getNowPlusMonth();

                $user->setResettingSecurityCode(NULL);
                $user->setPassword($passwordEncoder->encodePassword($user, $newPassword));
                $user->setPasswordExpiryDate($passwordExpiryDate);

                $em->merge($user);
                $em->flush();

                $this->addFlash('success', 'Twoje hasło zostało pomyślnie zmienione. Zaloguj się używając nowego hasła');

                return $this->redirectToRoute('login');
            }
            return $this->render("@SecurityModule/password_change.html.twig", array('form' => $form->createView()));
        }
    }

}