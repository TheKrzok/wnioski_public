<?php

namespace App\Application\SecurityModule\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Kontroler indeksu
 *
 * Class IndexController
 * @package App\Controller\SecurityModule
 */
class IndexController extends AbstractController
{
    /**
     * Akcja prezekierowania do strony logowania
     * gdy użytkownik wejdzie na stronę główną
     *
     * @Route("/", name="index")
     * @return Response
     */
    public function indexAction() {
        return $this->redirectToRoute('login');
    }
}