<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 19.01.2018
 * Time: 11:45
 */

namespace App\Application\SecurityModule\Controller;

use App\Entity\User;
use App\Application\SecurityModule\Form\PasswordExpiredType;
use App\Service\DateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *
 * Kontroler akcji zmiany hasła, które wygasło
 *
 * Class PasswordExpiredController - Kontroler
 * @package App\Controller\SecurityModule
 */
class PasswordExpiredController extends AbstractController
{
    /**
     * Akcja amiany hasła po wpisaniu przez użytkownika obecnego hasła oraz nowego hasła
     *
     * @Route("security/password_expired", name="password_expired")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param DateService $dateService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function passwordExpiredAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, DateService $dateService)
    {
        $form = $this->createForm(PasswordExpiredType::class)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $username       = $form->get('username')->getData();
            $em             = $this->getDoctrine()->getManager();
            $user           = $em->getRepository(User::class)->findOneBy(['username' => $username]);

            if($user) {
                $oldPassword            = $form->get('oldPassword')->getData();
                $newPassword            = $form->get('newPassword')->getData();
                $validateOldPassword    = $passwordEncoder->isPasswordValid($user, $oldPassword);

                if($validateOldPassword) {
                    if($oldPassword === $newPassword) {
                        $this->addFlash('error', 'Nowe hasło nie może być takie samo jak stare');
                    } else {

                        $passwordExpiryDate = $dateService->getNowPlusMonth();

                        $user->setPasswordExpiryDate($passwordExpiryDate);
                        $user->setPassword($passwordEncoder->encodePassword($user, $newPassword));

                        $em->merge($user);
                        $em->flush();

                        $this->addFlash('success', 'Pomyślnie zmieniono hasło. Możesz się zalogować używając nowych danych');

                        return $this->redirectToRoute("login");
                    }
                } else {
                    $this->addFlash('error', 'Podane stare hasło jest niepoprawne');
                }
            } else {
                $this->addFlash('error', 'Podana nazwa użytkownika jest błędna');
            }
        }

        return $this->render("@SecurityModule/password_expired.html.twig", array("form" => $form->createView()));
    }
}