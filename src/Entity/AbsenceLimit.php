<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AbsenceLimitRepository")
 */
class AbsenceLimit
{

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @ManyToMany(targetEntity="App\Entity\User", mappedBy="absenceLimit")
     * @JoinTable(name="users_absences_limits")
     *
     * @var User[]|ArrayCollection
     */
    private $user;

    /**
     * @return User[]|ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User[]|ArrayCollection $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AbsenceDict")
     * @JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;


    /**
     * @ORM\Column(name="year", type="integer", nullable=true)
     * @var int|null
     */
    private $year;


    /**
     * @ORM\Column(name="limit_value", type="integer", nullable=true)
     * @var integer
     */
    private $limitValue;

    public function getType()
    {
        return $this->type;
    }


    public function setType($typeId): void
    {
        $this->type = $typeId;
    }

    /**
     * @return int|null
     */
    public function getLimitValue(): ?int
    {
        return $this->limitValue;
    }

    /**
     * @param int|null $limitValue
     */
    public function setLimitValue(?int $limitValue): void
    {
        $this->limitValue = $limitValue;
    }

    /**
     * @return int|null
     */
    public function getYear(): ?int
    {
        return $this->year;
    }

    /**
     * @param int|null $year
     */
    public function setYear(?int $year): void
    {
        $this->year = $year;
    }
}
