<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleDictRepository")
 */
class RoleDict
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=20)
     * @var string
     */
    private $roleDescription;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getRoleDescription(): string
    {
        return $this->roleDescription;
    }

    /**
     * @param string $roleDescription
     */
    public function setRoleDescription(string $roleDescription): void
    {
        $this->roleDescription = $roleDescription;
    }
}
