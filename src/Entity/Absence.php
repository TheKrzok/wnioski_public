<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AbsenceRepository")
 */
class Absence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", inversedBy="absence")
     *
     * @var User|null
     */
    private $user;

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @ManyToOne(targetEntity="App\Entity\AbsenceDict")
     * @var AbsenceDict|null
     */
    private $type;

    /**
     * @ManyToOne(targetEntity="App\Entity\AbsenceStatusDict")
     * @var AbsenceStatusDict
     */
    private $status;

    /**
     * @ORM\Column(name="from_date", type="date", nullable=false)
     * @var DateTime|null
     */
    private $fromDate;

    /**
     * @ORM\Column(name="to_date", type="date", nullable=false)
     * @var DateTime|null
     */
    private $toDate;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int|null
     */
    private $days;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private $year;

    /**
     * @ORM\Column(name="security_code", type="string", length=255)
     * @var string|null
     */
    private $securityCode;

    /**
     * @ORM\Column(name="status_change_date", type="datetime", nullable=true)
     * @var DateTime|null
     */
    private $statusChangeDate;

    /**
     * @ORM\Column(name="rejected_reason", type="string", length=255, nullable=true)
     * @var string|null
     */
    private $rejectedReason;

    /**
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     * @var DateTime|null
     */
    private $createDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return AbsenceDict|null
     */
    public function getType(): ?AbsenceDict
    {
        return $this->type;
    }

    /**
     * @param AbsenceDict|null $type
     */
    public function setType(?AbsenceDict $type): void
    {
        $this->type = $type;
    }

    /**
     * @return AbsenceStatusDict
     */
    public function getStatus(): AbsenceStatusDict
    {
        return $this->status;
    }

    /**
     * @param AbsenceStatusDict $status
     */
    public function setStatus(AbsenceStatusDict $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getDays(): ?int
    {
        return $this->days;
    }

    /**
     * @param int|null $days
     */
    public function setDays(?int $days): void
    {
        $this->days = $days;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return DateTime|null
     */
    public function getFromDate(): ?DateTime
    {
        return $this->fromDate;
    }

    /**
     * @param DateTime|null $fromDate
     */
    public function setFromDate(?DateTime $fromDate): void
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return DateTime|null
     */
    public function getToDate(): ?DateTime
    {
        return $this->toDate;
    }

    /**
     * @param DateTime|null $toDate
     */
    public function setToDate(?DateTime $toDate): void
    {
        $this->toDate = $toDate;
    }

    /**
     * @return null|string
     */
    public function getSecurityCode(): ?string
    {
        return $this->securityCode;
    }

    /**
     * @param null|string $securityCode
     */
    public function setSecurityCode(?string $securityCode): void
    {
        $this->securityCode = $securityCode;
    }

    /**
     * @return DateTime|null
     */
    public function getStatusChangeDate(): ?DateTime
    {
        return $this->statusChangeDate;
    }

    /**
     * @param DateTime|null $statusChangeDate
     */
    public function setStatusChangeDate(?DateTime $statusChangeDate): void
    {
        $this->statusChangeDate = $statusChangeDate;
    }

    /**
     * @return null|string
     */
    public function getRejectedReason(): ?string
    {
        return $this->rejectedReason;
    }

    /**
     * @param null|string $rejectedReason
     */
    public function setRejectedReason(?string $rejectedReason): void
    {
        $this->rejectedReason = $rejectedReason;
    }

    /**
     * @return DateTime|null
     */
    public function getCreateDate(): ?DateTime
    {
        return $this->createDate;
    }

    /**
     * @param DateTime|null $createDate
     */
    public function setCreateDate(?DateTime $createDate): void
    {
        $this->createDate = $createDate;
    }
}
