<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Encja zawierająca role dla danego użytkownika
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRoleRepository")
 */
class UserRole
{
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ManyToMany(targetEntity="App\Entity\User", mappedBy="userRole")
     *
     * @var User[]|ArrayCollection
     */
    private $user;

    /**
     * @ORM\Column(name="role", type="string", length=30, nullable=false)
     * @var string
     */
    private $role;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return User[]
     */
    public function getUser()
    {
        return $this->user;
    }
}
