<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 20.01.2018
 * Time: 14:18
 */

namespace App\Twig;

use Twig_Extension;

/**
 * Klasa stworzona na potrzeby sformatowania wygasania hasła
 *
 * Class DateExtension
 * @package App\Twig
 */
class DateExtension extends Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter("expireDate", [$this, "expireDate"])
        ];
    }

    /**
     * @param \DateTime $expiresAt
     *
     * @return string
     */
    public function expireDate(\DateTime $expiresAt)
    {
        if ($expiresAt < new \DateTime("-7 days")) {
            return $expiresAt->format("Y-m-d H:i");
        }

        if ($expiresAt > new \DateTime("-1 day")) {
            return "za " . $expiresAt->diff(new \DateTime())->days . " dni";
        }

        return "za " . $expiresAt->format("H") . " godz. " . $expiresAt->format("i") . " min.";
    }
}