<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 11:19
 */

namespace App\Twig;

use Twig_Extension;

class AbsenceExtension extends Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction("statusStyle", [$this, "statusStyle"])
        ];
    }

    /**
     * @param string $status
     * @return string
     */
    public function statusStyle(string $status)
    {
        if ($status === 'accepted') {
            return "text-success";
        } else if($status === 'rejected') {
            return "text-danger";
        }
        return "";
    }
}